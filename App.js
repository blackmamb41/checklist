import React from 'react';
import { Platform, StatusBar, StyleSheet, View } from 'react-native';
import { AppLoading, Asset, Font, Icon } from 'expo';
import AppNavigator from './navigation/AppNavigator';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import { Root } from 'native-base'
import Assignment from './pages/Assignment'
import Login from './pages/Login'
import Splash from './pages/Splash'
import Checked from './pages/Checked'
import Checking from './pages/Checking'
import Profile from './pages/Profile'

const Main = createStackNavigator({
    Assignment: {
      screen: Assignment
    },
    Checked: {
      screen: Checked
    },
    Login: {
      screen: Login,
      navigationOptions: {
        mode: 'modal',
        header: null,
        headerMode: 'none',
      }
    },
    Splash: {
      screen: Splash,
      navigationOptions: {
        mode: 'modal',
        header: null,
        headerMode: 'none',
      }
    },
    Checking: {
      screen: Checking
    },
    Profile: {
      screen: Profile
    }
}, {
  initialRouteName: 'Splash'
})

const MainMenu = createAppContainer(Main)

// export default createAppContainer(Main)
export default () =>
  <Root>
    <MainMenu />
  </Root>