import React from 'react'
import { Text } from 'react-native'
import { 
    Container, 
    Header, 
    Content, 
    Footer, 
    FooterTab,
    Button,
    StyleProvider
} from 'native-base'
import getTheme from '../native-base-theme/components';
import material from '../native-base-theme/variables/material';
import { StackActions, NavigationActions, withNavigation } from 'react-navigation'

class FooterMenu extends React.Component {
    componentDidMount() {        
    }

    goToAssignment = () => {
        this.props.navigation.dispatch(StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({routeName: 'Assignment'})
            ]
        }))
    }

    goToChecked = () => {
        this.props.navigation.dispatch(StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({routeName: 'Checked'})
            ]
        }))
    }
    render() {
        if(this.props.navigation.state.routeName == 'Assignment') {
            return (
                <StyleProvider style={getTheme(material)}>
                    <Footer>
                        <FooterTab style={{backgroundColor: '#c0392b'}}>
                            <Button active>
                                <Text>Assignment</Text>
                            </Button>
                            <Button onPress={this.goToChecked} >
                                <Text>Checked</Text>
                            </Button>
                        </FooterTab>
                    </Footer>
                </StyleProvider>
            )
        }else{
            return (
                <StyleProvider style={getTheme(material)}>
                    <Footer>
                        <FooterTab style={{backgroundColor: '#c0392b'}}>
                            <Button onPress={this.goToAssignment}>
                                <Text style={{color: '#fff'}}>Assignment</Text>
                            </Button>
                            <Button active>
                                <Text style={{color: '#fff'}}>Checked</Text>
                            </Button>
                        </FooterTab>
                    </Footer>
                </StyleProvider>
            )
        }
    }
}

export default withNavigation(FooterMenu)