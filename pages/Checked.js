import React from 'react'
import { View, AsyncStorage, Text, RefreshControl } from 'react-native'
import Axios from 'axios'
import {
    StyleProvider,
    Container,
    Content,
    List,
    ListItem,
    Left,
    Thumbnail,
    Body,
    Right,
    Spinner,
    Badge,
    Icon,
    Button,
    Toast
} from 'native-base'
import getTheme from '../native-base-theme/components';
import material from '../native-base-theme/variables/material';
import FooterMenu from '../components/FooterMenu'
import { ScrollView } from 'react-native-gesture-handler';
import { hostApi } from '../utils/Server';

export default class Checked extends React.Component {
    static navigationOptions = {
        title: 'Checked',
        headerStyle: {
            backgroundColor: '#c0392b'
        },
        headerTitleStyle: {
            color: '#fff',
            textAlign: 'center',
            flex: 1,
        }  
    }

    constructor(props) {
        super(props)
        this.state = {
            item: [],
            userData: {},
            loading: false,
            refresh: false
        }
    }

    async componentDidMount() {
        await AsyncStorage.getItem('user', (error, data) => {
            jsonData = JSON.parse(data)
            this.setState({userData: jsonData})
        })

        this.setState({loading: !this.state.loading})
        await Axios.get(hostApi+'/items/checked?id_user='+this.state.userData.id)
            .then(response => {
                this.setState({
                    item: response.data,
                    loading: !this.state.loading
                })
            }).catch(error => {
                console.log(error)
            })
        await Expo.Font.loadAsync({
            Roboto: require("native-base/Fonts/Roboto.ttf"),
            Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
            Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf"),
        });

    }

    renderListItem() {
        if(this.state.loading == true) {
            return(<Spinner color='green' />)
        }else{
            if(this.state.item.length == 0) {
                return (<Text>No Item Checked</Text>)
            }else{
                let checkedItem = this.state.item.map((item) => {
                    if(item.type == 'ups') {
                        imgThumbnail = <Thumbnail square source={require('../assets/images/power.png')}></Thumbnail>
                    }else if(item.type == 'switch' || item.type == 'router') {
                        imgThumbnail = <Thumbnail square source={require('../assets/images/aruba.png')}></Thumbnail>
                    }else if(item.type == 'server') {
                        imgThumbnail = <Thumbnail square source={require('../assets/images/server.png')}></Thumbnail>
                    }else if(item.type == 'ac') {
                        imgThumbnail = <Thumbnail square source={require('../assets/images/ac.png')}></Thumbnail>
                    }else if(item.type == 'cctv') {
                        imgThumbnail = <Thumbnail square source={require('../assets/images/cctv.png')}></Thumbnail>
                    }else if(item.type == 'wireless') {
                        imgThumbnail = <Thumbnail square source={require('../assets/images/wifi.png')}></Thumbnail>
                    }
    
                    if(item.site == 'rosuta') {
                        siteName = <Badge primary><Text style={{color: '#fff'}}>Gd. SCS Regional Jabar</Text></Badge>
                    }else if(item.site == 'ttcust') {
                        siteName = <Badge primary><Text style={{color: '#fff'}}>TTC Suta Regional Jabar</Text></Badge>
                    }else if(item.site == 'cirebon') {
                        siteName = <Badge primary><Text style={{color: '#fff'}}>Branch Cirebon</Text></Badge>
                    }else if(item.site == 'tasik') {
                        siteName = <Badge primary><Text style={{color: '#fff'}}>Branch Tasikmalaya</Text></Badge>
                    }else if(item.site == 'cianjur') {
                        siteName = <Badge primary><Text style={{color: '#fff'}}>Grapari Cianjur</Text></Badge>
                    }else if(item.site == 'windu') {
                        siteName = <Badge primary><Text style={{color: '#fff'}}>Branch Windu</Text></Badge>
                    }else if(item.site == 'soreang') {
                        siteName = <Badge primary><Text style={{color: '#fff'}}>Branch Soreang</Text></Badge>
                    }
                    console.log(item)
                    return (
                        <List style={{backgroundColor: '#fff', elevation: 1}}>
                            <ListItem thumbnail>
                                <Left>
                                    {imgThumbnail}
                                </Left>
                                <Body>
                                    <Text>{item.NE}</Text>
                                    {siteName}
                                    <Text>{item.floor}</Text>
                                </Body>
                            </ListItem>
                        </List>
                    )
                })
                return checkedItem
            }
        }
    }
    
    _onRefresh = () => {
        this.setState({refresh: !this.state.refresh})
        Axios.get(hostApi+'/items/checked?id_user='+this.state.userData.id)
            .then(response => {
                this.setState({
                    item: response.data,
                    refresh: !this.state.refresh
                })
            }).catch(error => {
                console.log(error)
            })
    }

    buttonRender(){
        if(this.state.item.length == 0){
            return(<View></View>)
        }else{
            return(
                <Button block info onPress={this._shareOnTelegramGroup}>
                    <Icon name="paper-plane" />
                    <Text style={{color: "#fff"}}>Report On Telegram</Text>
                </Button>
            )
        }
    }

    _shareOnTelegramGroup = () => {
        Axios.get(hostApi+'/report/send?id_user='+this.state.userData.id)
            .then(reponse => {
                Toast.show({
                    text: 'Sending Report Successfully',
                    buttonText: 'Okay'
                })
            }).catch(error => {
                Toast.show({
                    text: 'Oops Something Wrong',
                    buttonText: 'Okay'
                })
            })
    }

    render() {
        return (
            <StyleProvider style={getTheme(material)}>
                <Container style={{backgroundColor: '#dfe6e9'}}>
                    <Content style={{margin: '3%'}}>
                        <ScrollView
                            refreshControl={
                                <RefreshControl 
                                    refreshing={this.state.refresh}
                                    onRefresh={this._onRefresh}
                                />
                            }
                        >
                            {this.renderListItem()}
                            {this.buttonRender()}
                        </ScrollView>
                    </Content>
                    <FooterMenu />
                </Container>
            </StyleProvider>
        )
    }
}