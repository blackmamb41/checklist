import React from 'react'
import { View, Image, StyleSheet, AsyncStorage } from 'react-native'
import { Grid, Row, Col } from 'react-native-easy-grid'
import { StackActions, NavigationActions } from 'react-navigation';
import { SplashScreen } from 'expo'

const style = StyleSheet.create({
    bgLogin: {
        backgroundColor: '#fff',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        width: '100%'
    }
})

export default class Splash extends React.Component {
    componentDidMount() {
        SplashScreen.hide();
        AsyncStorage.getItem('user', (error, data) => {
            if(data == null) {
                this.props.navigation.dispatch(StackActions.reset({
                    index: 0,
                    actions: [
                        NavigationActions.navigate({routeName: 'Login'})
                    ]
                }))
            }else{
                this.props.navigation.dispatch(StackActions.reset({
                    index: 0,
                    actions: [
                        NavigationActions.navigate({routeName: 'Assignment'})
                    ]
                }))
            }
        })
    }
    render() {
        return (
            <View style={style.bgLogin}>
                <Grid>
                    <Row size={3} style={{flex: 1, flexDirection: 'column-reverse', alignItems: 'center'}}>
                        <Image source={require('../assets/images/telkomsel.png')} style={{width: 150, height: 150, alignItems: 'center'}} />
                    </Row>
                    <Row size={0.8}></Row>
                </Grid>
            </View>
        )
    }
}