import React from 'react'
import { View, StyleSheet, AsyncStorage, Text } from 'react-native'
import { 
    Content, 
    Container, 
    StyleProvider, 
    CardItem,
    Thumbnail,
    Body,
    Card,
    Badge,
    Item,
    Input,
    Textarea,
    Form,
    Button,
    Toast
} from 'native-base'
import getTheme from '../native-base-theme/components';
import material from '../native-base-theme/variables/material';
import { Row, Grid, Col } from 'react-native-easy-grid'
import 
    RadioForm, { 
    RadioButton, 
    RadioButtonInput, 
    RadioButtonLabel 
} from 'react-native-simple-radio-button'
import Axios from 'axios'
import { hostApi } from '../utils/Server';
import Hr from 'react-native-hr-plus';
import ListItem from '../native-base-theme/components/ListItem';
import { unserialize } from "locutus/php/var";
import { MaterialDialog } from 'react-native-material-dialog';
import Loader from 'react-native-modal-loader';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { NavigationActions } from 'react-navigation';

var statusitem = [
    {label: 'NOK', value: 0},
    {label: 'OK', value: 1},
]

export default class Checking extends React.Component {
    static navigationOptions = {
        title: 'Checking',
        headerStyle: {
            backgroundColor: '#c0392b'
        },
        headerTitleStyle: {
            color: '#fff',
            textAlign: 'center',
            flex: 1,
        },
        headerTintColor: '#fff'
    }

    constructor(props) {
        super(props)
        this.state = {
            id_user: '',
            item: {},
            status: 0,
            R: '',
            S: '',
            T: '',
            upload: '',
            download: '',
            humadity: '',
            temperature: '',
            ping: '',
            optional: '',
            loading: false,
            error: false,
            showToast: false,
            message: '',
            ping: 0,
        }
    }

    async componentDidMount() {
        let data = await AsyncStorage.getItem('singleItem');
        let jsonData = JSON.parse(data)
        this.setState({item: jsonData})
        let userData = await AsyncStorage.getItem('user')
        let jsonUser = JSON.parse(userData)
        this.setState({id_user: jsonUser.id})
        await Expo.Font.loadAsync({
            Roboto: require("native-base/Fonts/Roboto.ttf"),
            Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
            Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf"),
        });
    }

    renderSingleItemLayout() {
        let item = this.state.item
        if(item.type == 'ups') {
            imgThumbnail = <Thumbnail square source={require('../assets/images/power.png')}></Thumbnail>
        }else if(item.type == 'switch' || item.type == 'router') {
            imgThumbnail = <Thumbnail square source={require('../assets/images/aruba.png')}></Thumbnail>
        }else if(item.type == 'server') {
            imgThumbnail = <Thumbnail square source={require('../assets/images/server.png')}></Thumbnail>
        }else if(item.type == 'ac') {
            imgThumbnail = <Thumbnail square source={require('../assets/images/ac.png')}></Thumbnail>
        }else if(item.type == 'cctv') {
            imgThumbnail = <Thumbnail square source={require('../assets/images/cctv.png')}></Thumbnail>
        }else if(item.type == 'wireless') {
            imgThumbnail = <Thumbnail square source={require('../assets/images/wifi.png')}></Thumbnail>
        }

        if(item.site == 'rosuta') {
            siteName = <Badge primary><Text style={{color: '#fff'}}>Gd. SCS Regional Jabar</Text></Badge>
        }else if(item.site == 'ttcust') {
            siteName = <Badge primary><Text style={{color: '#fff'}}>TTC Suta Regional Jabar</Text></Badge>
        }else if(item.site == 'cirebon') {
            siteName = <Badge primary><Text style={{color: '#fff'}}>Branch Cirebon</Text></Badge>
        }else if(item.site == 'tasik') {
            siteName = <Badge primary><Text style={{color: '#fff'}}>Branch Tasikmalaya</Text></Badge>
        }else if(item.site == 'cianjur') {
            siteName = <Badge primary><Text style={{color: '#fff'}}>Grapari Cianjur</Text></Badge>
        }else if(item.site == 'windu') {
            siteName = <Badge primary><Text style={{color: '#fff'}}>Branch Windu</Text></Badge>
        }else if(item.site == 'soreang') {
            siteName = <Badge primary><Text style={{color: '#fff'}}>Branch Soreang</Text></Badge>
        }

        return(
            <Card style={{elevation: 2}}>
                <CardItem>
                    {imgThumbnail}
                    <Body style={{marginLeft: '10%'}}>
                        <Text>{this.state.item.NE}</Text>
                        {siteName}
                    </Body>
                </CardItem>
            </Card>
        )
    }

    _getAllItems() {
        Axios.get(hostApi+'/items?id_user='+this.state.id_user)
            .then(response => {
                // this.setState({
                //     item: response.data,
                //     loading: !this.state.loading
                // })
                AsyncStorage.setItem('assignmentItems', JSON.stringify(response.data))
            }).catch(error => {
                console.log(error)
            })
    }

    _onSubmit = async () => {
        console.log(this.state.status)
        const today = new Date()
        let timeNow = today.getHours()+":"+today.getMinutes()+":"+today.getSeconds()
        if(this.state.item.type == 'ups') {
            if(this.state.R == '' || this.state.S == '' || this.state.T == '') {
                Toast.show({
                    text: 'Data Belum Lengkap',
                    buttonText: "Okay",
                    type: 'danger'
                })
            }else{
                this.setState({loading: !this.state.loading})
                Axios.post(hostApi+'/report/checking/insert?codename='+this.state.item.codename, {
                    status: this.state.status,
                    time: timeNow,
                    data: {
                        r: this.state.R,
                        s: this.state.S,
                        t: this.state.T
                    },
                    enginer: this.state.id_user
                }).then(response => {
                    this.setState({
                        message: 'Report Checking Successfully'
                    })
                    Toast.show({
                        text: this.state.message,
                        buttonText: 'Okay',
                        type: 'success'
                    })
                    this.props.navigation.replace('Assignment')
                }).catch(error => {
                    this.setState({
                        message: 'Something Wrong With Connection'
                    })
                    Toast.show({
                        text: this.state.message,
                        buttonText: 'Okay',
                        type: 'danger'
                    })
                    console.log(error)
                })
                this.setState({loading: false})
            }
        }else if(this.state.item.type == 'ac') {
            if(this.state.humadity == '' || this.state.temperature == '') {
                Toast.show({
                    text: 'Data Belum Lengkap',
                    buttonText: "Okay",
                    type: 'danger'
                })
            }else{
                this.setState({loading: !this.state.loading})
                Axios.post(hostApi+'/report/checking/insert?codename='+this.state.item.codename, {
                    status: this.state.status,
                    time: timeNow,
                    data: {
                        humadity: this.state.humadity,
                        temperature: this.state.temperature,
                    },
                    enginer: this.state.id_user
                }).then(response => {
                    this.setState({
                        message: 'Report Checking Successfully'
                    })
                    Toast.show({
                        text: this.state.message,
                        buttonText: 'Okay',
                        type: 'success'
                    })
                    this.props.navigation.replace('Assignment')
                }).catch(error => {
                    this.setState({
                        message: 'Something Wrong With Connection'
                    })
                    Toast.show({
                        text: this.state.message,
                        buttonText: 'Okay',
                        type: 'danger'
                    })
                    console.log(error)
                })
                this.setState({loading: false})
            }
        }else if(this.state.item.type == 'wireless') {
            if(this.state.upload == '' || this.state.download == '') {
                Toast.show({
                    text: 'Data Belum Lengkap',
                    buttonText: "Okay",
                    type: 'danger'
                })
            }else{
                this.setState({loading: !this.state.loading})
                Axios.post(hostApi+'/report/checking/insert?codename='+this.state.item.codename, {
                    status: this.state.status,
                    time: timeNow,
                    data: {
                        upload: this.state.upload,
                        download: this.state.download
                    },
                    enginer: this.state.id_user
                }).then(response => {
                    this.setState({
                        message: 'Report Checking Successfully'
                    })
                    Toast.show({
                        text: this.state.message,
                        buttonText: 'Okay',
                        type: 'success'
                    })
                    this._getAllItems()
                    this.props.navigation.replace('Assignment')
                }).catch(error => {
                    this.setState({
                        message: 'Something Wrong With Connection'
                    })
                    Toast.show({
                        text: this.state.message,
                        buttonText: 'Okay',
                        type: 'danger'
                    })
                    console.log(error)
                })
                this.setState({loading: false})
            }
        }else if(this.state.item.type == 'switch' || this.state.item.type == 'router' || this.state.item.type == 'server'){
            if(this.state.temperature == ''){
                Toast.show({
                    text: 'Data Belum Lengkap',
                    buttonText: "Okay",
                    type: 'danger'
                })
            }else{
                this.setState({loading: !this.state.loading})
                Axios.post(hostApi+'/report/checking/insert?codename='+this.state.item.codename, {
                    status: this.state.status,
                    time: timeNow,
                    data: {
                        ping: this.state.ping,
                        temperature: this.state.temperature
                    },
                    enginer: this.state.id_user
                }).then(response => {
                    this.setState({
                        message: 'Report Checking Successfully'
                    })
                    Toast.show({
                        text: this.state.message,
                        buttonText: 'Okay',
                        type: 'success'
                    })
                    this._getAllItems()
                    this.props.navigation.reset([NavigationActions.navigate({ routeName: 'Assignment' })], 0)
                }).catch(error => {
                    this.setState({
                        message: 'Something Wrong With Connection'
                    })
                    Toast.show({
                        text: this.state.message,
                        buttonText: 'Okay',
                        type: 'danger'
                    })
                    console.log(error)
                })
                this.setState({loading: false})
            }
        }else{
            if(this.state.temperature == ''){
                Toast.show({
                    text: 'Data Belum Lengkap',
                    buttonText: "Okay",
                    type: 'danger'
                })
            }else{
                this.setState({loading: !this.state.loading})
                Axios.post(hostApi+'/report/checking/insert?codename='+this.state.item.codename, {
                    status: this.state.status,
                    time: timeNow,
                    data: {
                        ping: this.state.ping,
                        temperature: this.state.temperature
                    },
                    enginer: this.state.id_user
                }).then(response => {
                    this.setState({
                        message: 'Report Checking Successfully'
                    })
                    Toast.show({
                        text: this.state.message,
                        buttonText: 'Okay',
                        type: 'success'
                    })
                    this._getAllItems()
                    this.props.navigation.reset([NavigationActions.navigate({ routeName: 'Assignment' })], 0)
                }).catch(error => {
                    this.setState({
                        message: 'Something Wrong With Connection'
                    })
                    Toast.show({
                        text: this.state.message,
                        buttonText: 'Okay',
                        type: 'danger'
                    })
                    console.log(error)
                })
                this.setState({loading: false})
            }
        }
    }

    renderViewData() {
        if(this.state.item.type == 'ups') {
            return(
                <View>
                    <CardItem>
                        <Grid>
                            <Col>
                                <Text style={{textAlign: 'center'}}>Phase R :</Text>
                                <Item regular>
                                    <Input onChangeText={(r) => this.setState({R: r})} keyboardType='numeric'/>
                                </Item>
                            </Col>
                            <Col>
                                <Text style={{textAlign: 'center'}}>Phase S :</Text>
                                <Item regular>
                                    <Input onChangeText={(s) => this.setState({S: s})} keyboardType='numeric'/>
                                </Item>
                            </Col>
                            <Col>
                                <Text style={{textAlign: 'center'}}>Phase T :</Text>
                                <Item regular>
                                    <Input onChangeText={(t) => this.setState({T: t})} keyboardType='numeric'/>
                                </Item>
                            </Col>
                        </Grid>
                    </CardItem>
                    <CardItem>
                        <Grid>
                            <Row>
                                <Text>{'\n'}</Text>
                                <Text>Optional Information</Text>
                            </Row>
                            <Row>
                                <Form style={{width: '100%'}}>
                                    <Textarea rowSpan={3} bordered placeholder="Information" onChangeText={(info) => this.setState({optional: info})}/>
                                </Form>
                            </Row>
                        </Grid>
                    </CardItem>
                </View>
            )
        }else if(this.state.item.type == 'ac') {
            return(
                <View>
                    <CardItem>
                        <Grid>
                            <Col>
                                <Text style={{textAlign: 'center'}}>Humadity</Text>
                                <Item regular>
                                    <Input onChangeText={(humadity) => this.setState({humadity: humadity})} keyboardType='numeric'/>
                                </Item>
                            </Col>
                            <Col>
                                <Text style={{textAlign: 'center'}}>Temperature</Text>
                                <Item regular>
                                    <Input onChangeText={(temp) => this.setState({temperature: temp})} keyboardType='numeric'/>
                                </Item>
                            </Col>
                        </Grid>
                    </CardItem>
                    <CardItem>
                        <Grid>
                            <Row>
                                <Text>{'\n'}</Text>
                                <Text>Optional Information</Text>
                            </Row>
                            <Row>
                                <Form style={{width: '100%'}}>
                                    <Textarea rowSpan={3} bordered placeholder="Information" onChangeText={(info) => this.setState({optional: info})}/>
                                </Form>
                            </Row>
                        </Grid>
                    </CardItem>
                </View>
            )
        }else if(this.state.item.type == 'wireless'){
            return(
                <View>
                    <CardItem>
                        <Grid>
                            <Col>
                                <Text style={{textAlign: 'center'}}>Download</Text>
                                <Item regular>
                                    <Input onChangeText={(download) => this.setState({download: download})} keyboardType='numeric'/>
                                </Item>
                            </Col>
                            <Col>
                                <Text style={{textAlign: 'center'}}>Upload</Text>
                                <Item regular>
                                    <Input onChangeText={(upload) => this.setState({upload: upload})} keyboardType='numeric'/>
                                </Item>
                            </Col>
                        </Grid>
                    </CardItem>
                    <CardItem>
                        <Grid>
                            <Row>
                                <Text>{'\n'}</Text>
                                <Text>Optional Information</Text>
                            </Row>
                            <Row>
                                <Form style={{width: '100%'}}>
                                    <Textarea rowSpan={3} bordered placeholder="Information" onChangeText={(info) => this.setState({optional: info})}/>
                                </Form>
                            </Row>
                        </Grid>
                    </CardItem>
                </View>
            )
        }else if(this.state.item.type == 'switch' || this.state.item.type == 'router' || this.state.item.type == 'server'){
            return (
                <View>
                    <CardItem>
                        <Grid>
                            <Col>
                                <Text style={{textAlign: 'center'}}>Ping Status{'\n'}</Text>
                                <Row style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
                                    <RadioForm 
                                        radio_props={statusitem}
                                        initial={0}
                                        formHorizontal={true}
                                        labelHorizontal={false}
                                        buttonColor={'#c0392b'}
                                        animation={true}
                                        onPress={(value) => this.setState({ping: value})}
                                    />
                                </Row>
                                <Text style={{textAlign: 'center'}}>Temperature</Text>
                                <Item regular>
                                    <Input onChangeText={(temperature) => this.setState({temperature: temperature})} keyboardType='numeric' style={{width: '30%'}}/>
                                </Item>
                                <Grid>
                                    <Row>
                                        <Text>{'\n'}</Text>
                                        <Text>Optional Information</Text>
                                    </Row>
                                    <Row>
                                        <Form style={{width: '100%'}}>
                                            <Textarea rowSpan={3} bordered placeholder="Information" onChangeText={(info) => this.setState({optional: info})}/>
                                        </Form>
                                    </Row>
                                </Grid>
                            </Col>
                        </Grid>
                    </CardItem>
                </View>
            )
        }else {
            return (
                <View>
                    <CardItem>
                        <Grid>
                            <Col>
                                <Text style={{textAlign: 'center'}}>Ping Status{'\n'}</Text>
                                <Row style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
                                    <RadioForm 
                                        radio_props={statusitem}
                                        initial={0}
                                        formHorizontal={true}
                                        labelHorizontal={false}
                                        buttonColor={'#c0392b'}
                                        animation={true}
                                        onPress={(value) => this.setState({ping: value})}
                                    />
                                </Row>
                                <Text style={{textAlign: 'center'}}>Temperature</Text>
                                <Item regular>
                                    <Input onChangeText={(temperature) => this.setState({temperature: temperature})} keyboardType='numeric' style={{width: '30%'}}/>
                                </Item>
                                <Grid>
                                    <Row>
                                        <Text>{'\n'}</Text>
                                        <Text>Optional Information</Text>
                                    </Row>
                                    <Row>
                                        <Form style={{width: '100%'}}>
                                            <Textarea rowSpan={3} bordered placeholder="Information" onChangeText={(info) => this.setState({optional: info})}/>
                                        </Form>
                                    </Row>
                                </Grid>
                            </Col>
                        </Grid>
                    </CardItem>
                </View>
            )
        }
    }

    render() {
        return(
            <KeyboardAwareScrollView resetScrollToCoords={{x:0, y:0}} scrollEnabled={true} enableOnAndroid>
                <StyleProvider style={getTheme(material)}>
                    <Container style={{backgroundColor: '#dfe6e9'}}>
                        <Content style={{margin: '3%'}}>
                            {this.renderSingleItemLayout()}
                            <Hr color="#95a5a6" width={1}>
                                <Text>{'\u00A0'}</Text><Text style={{color: '#95a5a6'}}>{'\u00A0'}Requirement {'\u00A0'}</Text>
                            </Hr>
                            <Card>
                                <CardItem>
                                    <Row style={{flex: 1, flexDirection: 'row', justifyContent: 'center', marginBottom: -10}}>
                                        <Text style={{fontSize: 20, fontWeight: 'bold', textDecorationLine: 'underline'}}>STATUS</Text>
                                    </Row>
                                </CardItem>
                                <CardItem>
                                    <Row style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
                                        <RadioForm 
                                            radio_props={statusitem}
                                            initial={0}
                                            formHorizontal={true}
                                            labelHorizontal={false}
                                            buttonColor={'#c0392b'}
                                            animation={true}
                                            onPress={(value) => this.setState({status: value})}
                                        />
                                    </Row>
                                </CardItem>
                                <CardItem>
                                    <Row style={{flex: 1, flexDirection: 'row', justifyContent: 'center', marginVertical: -10}}>
                                        <Text style={{fontSize: 20, fontWeight: 'bold', textDecorationLine: 'underline'}}>RATING</Text>
                                    </Row>
                                </CardItem>
                                {this.renderViewData()}
                            </Card>
                            <Button block success onPress={this._onSubmit}>
                                <Text style={{color: '#fff'}}>SUBMIT</Text>
                            </Button>
                            <Loader loading={this.state.loading} />
                        </Content>
                    </Container>
                </StyleProvider>
            </KeyboardAwareScrollView>
        )
    }
}