import React from 'react'
import { Text, AsyncStorage, ScrollView, RefreshControl, TouchableOpacity } from 'react-native'
import { 
    Content,
    Container,
    StyleProvider,
    Button, 
    Card,
    CardItem,
    Thumbnail,
    Body,
    List,
    ListItem,
    Left,
    Right,
    Icon,
    Badge,
    Spinner
} from 'native-base';
import getTheme from '../native-base-theme/components';
import material from '../native-base-theme/variables/material';
import FooterMenu from '../components/FooterMenu'
import { Grid, Row } from 'react-native-easy-grid';
import Axios from 'axios';
import { hostApi } from '../utils/Server';
import Hr from 'react-native-hr-plus';

export default class Assignment extends React.Component {
    static navigationOptions = {
        title: 'Assignment',
        headerStyle: {
            backgroundColor: '#c0392b'
        },
        headerTitleStyle: {
            color: '#fff',
            textAlign: 'center',
            flex: 1,
        }  
    }

    constructor(props) {
        super(props)
        this.state = {
            item: [],
            userData: {},
            loading: false,
            refresh: false
        }
    }

    async componentDidMount() {
        await AsyncStorage.getItem('user', (error, data) => {
            jsonData = JSON.parse(data)
            this.setState({
                userData: jsonData
            })
        })

        this.setState({loading: !this.state.loading})
        await Axios.get(hostApi+'/items?id_user='+this.state.userData.id)
            .then(response => {
                // this.setState({
                //     item: response.data,
                //     loading: !this.state.loading
                // })
                AsyncStorage.setItem('assignmentItems', JSON.stringify(response.data))
            }).catch(error => {
                console.log(error)
            })
        await AsyncStorage.getItem('assignmentItems', (error, data) => {
            jsonItem = JSON.parse(data)
            this.setState({
                item: jsonItem,
                loading: !this.state.loading
            })
        })
    }

    setCodenameItem = (codenameItem) => {
        Axios.get(hostApi+'/items/single?codename='+codenameItem)
            .then(response => {
                AsyncStorage.setItem('singleItem', JSON.stringify(response.data))
                this.props.navigation.push('Checking')
            }).catch(error => {
                console.log(error)
            })
    }

    renderAssignment() {
        if(this.state.loading == true) {
            return(<Spinner color='green' />)
        }else {
            let itemAssignment = this.state.item.map((item) => {
                if(item.type == 'ups') {
                    imgThumbnail = <Thumbnail square source={require('../assets/images/power.png')}></Thumbnail>
                }else if(item.type == 'switch' || item.type == 'router') {
                    imgThumbnail = <Thumbnail square source={require('../assets/images/aruba.png')}></Thumbnail>
                }else if(item.type == 'server') {
                    imgThumbnail = <Thumbnail square source={require('../assets/images/server.png')}></Thumbnail>
                }else if(item.type == 'ac') {
                    imgThumbnail = <Thumbnail square source={require('../assets/images/ac.png')}></Thumbnail>
                }else if(item.type == 'cctv') {
                    imgThumbnail = <Thumbnail square source={require('../assets/images/cctv.png')}></Thumbnail>
                }else if(item.type == 'wireless') {
                    imgThumbnail = <Thumbnail square source={require('../assets/images/wifi.png')}></Thumbnail>
                }
    
                if(item.site == 'rosuta') {
                    siteName = <Badge primary><Text style={{color: '#fff'}}>Gd. SCS Regional Jabar</Text></Badge>
                }else if(item.site == 'ttcsuta') {
                    siteName = <Badge primary><Text style={{color: '#fff'}}>TTC Suta Regional Jabar</Text></Badge>
                }else if(item.site == 'cirebon') {
                    siteName = <Badge primary><Text style={{color: '#fff'}}>Branch Cirebon</Text></Badge>
                }else if(item.site == 'tasik') {
                    siteName = <Badge primary><Text style={{color: '#fff'}}>Branch Tasikmalaya</Text></Badge>
                }else if(item.site == 'cianjur') {
                    siteName = <Badge primary><Text style={{color: '#fff'}}>Grapari Cianjur</Text></Badge>
                }else if(item.site == 'windu') {
                    siteName = <Badge primary><Text style={{color: '#fff'}}>Branch Windu</Text></Badge>
                }else if(item.site == 'soreang') {
                    siteName = <Badge primary><Text style={{color: '#fff'}}>Branch Soreang</Text></Badge>
                }
                return (
                    <ListItem thumbnail>
                        <Left>
                            {imgThumbnail}
                        </Left>
                        <Body>
                            <Text>{item.NE}</Text>
                            {siteName}
                            <Text>Floor : {item.floor}</Text>
                        </Body>
                        <Right>
                            <Button transparent onPress={() => this.setCodenameItem(item.codename)}>
                                <Icon name="checkmark-circle-outline" style={{color: 'green'}}/><Text>CHECK</Text>
                            </Button>
                        </Right>
                    </ListItem>
                )
            })
            return itemAssignment
        }
    }

    _onRefresh = () => {
        this.setState({refresh: true});
        Axios.get(hostApi+'/items?id_user='+this.state.userData.id)
            .then(response => {
                this.setState({
                    refresh: false,
                    item: response.data
                })
            })
    }

    render() {
        return (
            <StyleProvider style={getTheme(material)}>
                <Container style={{backgroundColor: '#dfe6e9'}}>
                    <Content style={{margin: '3%'}}>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('Profile')}
                        >
                            <Card style={{elevation: 2}}>
                                <CardItem>
                                    <Thumbnail source={require('../assets/images/user.png')}/>
                                    <Body style={{marginLeft: '10%', marginTop: '2%'}}>
                                        <Text>{this.state.userData.name}</Text>
                                        <Text>{this.state.userData.role}</Text>
                                    </Body>
                                </CardItem>
                            </Card>
                        </TouchableOpacity>
                        <Text>{'\n'}</Text>
                        <Hr color="#95a5a6" width={1}>
                            <Text>{'\u00A0'}</Text><Text style={{color: '#95a5a6'}}>{'\u00A0'}Assignment {'\u00A0'}</Text>
                        </Hr>
                        <Text>{'\n'}</Text>
                        <List style={{backgroundColor: '#fff'}}>
                            <ScrollView
                                refreshControl={
                                    <RefreshControl 
                                        refreshing={this.state.refresh}
                                        onRefresh={this._onRefresh}
                                    />
                                }
                            >
                                {this.renderAssignment()}
                            </ScrollView>
                        </List>
                    </Content>
                    <FooterMenu />
                </Container>
            </StyleProvider>
        )
    }
}