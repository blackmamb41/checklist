import React from 'react'
import { View, Text, AsyncStorage } from 'react-native'
import { StyleProvider, Container, Button, Content, CardItem, Thumbnail, Body, Right, Left, Card } from 'native-base'
import getTheme from '../native-base-theme/components';
import material from '../native-base-theme/variables/material';
import Hr from 'react-native-hr-plus'
import { NavigationActions } from 'react-navigation';

export default class Profile extends React.Component {
    static navigationOptions = {
        title: 'Profile',
        headerStyle: {
            backgroundColor: '#c0392b'
        },
        headerTitleStyle: {
            color: '#fff',
            flex: 1,
        },
        headerTintColor: '#fff'
    }

    constructor(props) {
        super(props)
        this.state = {
            userData: {}
        }
    }

    async componentDidMount() {
        await AsyncStorage.getItem('user', (error, data) => {
            let jsonUser = JSON.parse(data)
            this.setState({userData: jsonUser})
        })
    }

    _doLogout = () => {
        AsyncStorage.removeItem('user')
        this.props.navigation.reset([NavigationActions.navigate({routeName: 'Login'})])
    }

    render() {
        return (
            <StyleProvider style={getTheme(material)}>
                <Container>
                    <Content>
                        <CardItem>
                            <Thumbnail source={require('../assets/images/user.png')} />
                            <Body style={{marginLeft: '10%', marginTop: '2%'}}>
                                <Text>{this.state.userData.name}</Text>
                                <Text>{this.state.userData.role}</Text>
                            </Body>
                        </CardItem>
                        <View style={{borderBottomColor: 'black',borderBottomWidth: 1, marginTop: '5%', marginBottom: '5%'}}/>
                        <CardItem>
                            <Left>
                                <Text>NIK :</Text>
                            </Left>
                            <Right>
                                <Text>{this.state.userData.nik}</Text>
                            </Right>
                        </CardItem>
                        <View style={{borderBottomColor: 'black',borderBottomWidth: 1, marginTop: '5%', marginBottom: '5%'}}/>
                        <CardItem>
                            <Left>
                                <Text>E-mail :</Text>
                            </Left>
                            <Right>
                                <Text>{this.state.userData.email}</Text>
                            </Right>
                        </CardItem>
                        <View style={{borderBottomColor: 'black',borderBottomWidth: 1, marginTop: '5%', marginBottom: '5%'}}/>
                        <CardItem>
                            <Left>
                                <Text>Role :</Text>
                            </Left>
                            <Right>
                                <Text>{this.state.userData.role}</Text>
                            </Right>
                        </CardItem>
                        <View style={{marginTop: '5%', marginBottom: '5%'}}/>
                        <Button danger block onPress={this._doLogout}><Text style={{color: '#fff'}}>LOGOUT</Text></Button>
                    </Content>
                </Container>
            </StyleProvider>
        )
    }
}