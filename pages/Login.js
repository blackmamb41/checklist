import React, {Fragment} from 'react'
import {
    View,
    Text,
    Image,
    StyleSheet,
    TouchableOpacity,
    AsyncStorage,
} from 'react-native'
import Axios from 'axios'
import {
    Grid,
    Row,
    Col
} from 'react-native-easy-grid'
import {
    Container,
    Content,
    Form,
    Item,
    Input,
    Label,
    Icon,
    Button,
    Spinner
} from 'native-base'
import GradientButton from 'react-native-gradient-buttons'
import { MaterialDialog } from 'react-native-material-dialog'
import { LinearGradient } from 'expo';
import { hostApi } from '../utils/Server'
import { NavigationActions, StackActions } from 'react-navigation'
import Overlay from 'react-native-modal-overlay'

const style = StyleSheet.create({
    bgLogin: {
        backgroundColor: '#fff',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center'
    }
})

export default class Login extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            nik: '',
            password: '',
            loading: false,
            error: false,
            errorMessage: '',
        }
    }

    loginAction = () => {
        this.setState({
            loading: !this.state.loading
        })
        Axios.post(hostApi+'/user/login', {
            nik: this.state.nik,
            password: this.state.password
        }).then(response => {
            AsyncStorage.setItem('user', JSON.stringify(response.data))
            this.setState({
                loading: !this.state.loading
            })
            this.props.navigation.reset([NavigationActions.navigate({routeName: 'Assignment'})])
        }).catch(error => {
            if(error.response.status) {
                this.setState({
                    error: !this.state.error,
                    errorMessage: 'NIK atau Password Anda Salah',
                    loading: !this.state.loading
                })
            }
        })
    }

    renderLoadingOrNot() {
        if(this.state.error == false) {
            return (<Text style={{color: '#fff', textAlign: 'center', fontSize: 18, marginTop: '3%', marginBottom: '3%'}}>LOGIN</Text>)
        }else{
            return (<Spinner color='#fff' size={3} />)
        }
    }

    render() {
        return (
            <View style={style.bgLogin}>
                <Overlay visible={this.state.loading} 
                    animationType="zoomIn" 
                    containerStyle={{backgroundColor: 'rgba(204,204,204,0.2)'}}
                    childrenWrapperStyle={{backgroundColor: '#eee'}}
                    animationDuration={500}>
                    {(hideModal, overlayState) => (
                        <Fragment>
                            <Spinner color='green'></Spinner>
                            <Text>Please Wait...</Text>
                        </Fragment>
                    )}
                </Overlay>
                <MaterialDialog
                    title='Error'
                    visible={this.state.error}
                    onOk={() => this.setState({error: !this.state.error})}
                    onCancel={() => this.setState({error: !this.state.error})}
                >
                    <Text>{this.state.errorMessage}</Text>
                </MaterialDialog>
                <Grid>
                    <Row size={3} style={{flex: 1, flexDirection: 'column-reverse', alignItems: 'center'}}>
                        <Image source={require('../assets/images/telkomsel.png')} style={{width: 150, height: 150}}/>
                    </Row>
                    <Row size={2} style={{marginTop: '10%'}}>
                        <Container>
                            <Content>
                                <Item style={{marginLeft: '15%', marginRight: '15%'}}>
                                    <Icon name='people' style={{color: 'grey'}}/>
                                    <Input keyboardType='numeric' placeholder='NIK' onChangeText={(nik) => this.setState({nik})} />
                                </Item>
                                <Text>{"\n"}</Text>
                                <Item style={{marginLeft: '15%', marginRight: '15%'}}>
                                    <Icon name='key' style={{color: 'grey'}}/>
                                    <Input secureTextEntry={true} placeholder='Password' onChangeText={(password) => this.setState({password})} />
                                </Item>
                                <TouchableOpacity onPress={this.loginAction} style={{marginLeft: '12%', marginRight: '12%', marginTop: '10%', borderRadius: 30, height: 30}}>
                                    <LinearGradient
                                            start={[0, 0.5]}
                                            end={[1, 0.5]}
                                            colors={['#DA251D', '#1f1c18']}
                                            style={{borderRadius: 30}}
                                        >
                                    {this.renderLoadingOrNot()}
                                    </LinearGradient>
                                </TouchableOpacity>
                                <Text>{'\n'}</Text>
                                <Text style={{color: 'red', flex: 1, textAlign: 'center'}}>Lupa Password ?</Text>
                            </Content>
                        </Container>
                    </Row>
                </Grid>
                <Text style={{justifyContent: 'center', textAlign: 'center', color: '#000', fontSize: 15}}>{'\u00A9'} IT Support Jawa Barat {'\u00A9'}</Text>
            </View>
        )
    }
}